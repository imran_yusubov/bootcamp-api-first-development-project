package com.api.first.develomnet.rest;

import com.api.first.develomnet.rest.service.CustomerCreator;
import com.api.first.development.openapi.api.CustomerApi;
import com.api.first.development.openapi.model.Customer;
import com.api.first.development.openapi.model.CustomerRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequiredArgsConstructor
public class CustomerRestApi implements CustomerApi {

    private final CustomerCreator customerCreator;

    @Override
    public ResponseEntity<Customer> createCustomer(@Valid CustomerRequest customerRequest) {
        final Customer customer = customerCreator.createCustomer(customerRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(customer);
    }
}
