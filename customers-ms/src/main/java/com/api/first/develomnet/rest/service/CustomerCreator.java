package com.api.first.develomnet.rest.service;


import com.api.first.development.openapi.model.Customer;
import com.api.first.development.openapi.model.CustomerRequest;

public interface CustomerCreator {

    Customer createCustomer(CustomerRequest customerRequest);
}
