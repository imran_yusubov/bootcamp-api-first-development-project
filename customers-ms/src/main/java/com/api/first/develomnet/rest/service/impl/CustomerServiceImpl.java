package com.api.first.develomnet.rest.service.impl;

import com.api.first.develomnet.rest.service.CustomerCreator;
import com.api.first.development.openapi.model.Customer;
import com.api.first.development.openapi.model.CustomerRequest;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerCreator {

    @Override
    public Customer createCustomer(CustomerRequest customerRequest) {
        return new Customer(1,
                customerRequest.getFirstName(),
                customerRequest.getLastName(),
                customerRequest.getAge(),
                customerRequest.getType()
        );
    }
}
